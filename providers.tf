provider "azurerm" {
  features {}
  // Optional kann die Subscription angegeben werden, sofern mehrere vorhanden sind
  // subscription_id = "49f**-**-**-**-**"

  client_id     = "4f190537-****-4bb4-95d5-****"
  client_secret = var.client_secret
  tenant_id     = "75d05689-****-4ae8-aa45-****"
}
