variable "infix" {
  type        = string
  default     = "cla-tf-demo1"
  description = "Infix for naming resources"
}

variable "location" {
  type        = string
  default     = "westeurope"
  description = "Default Azure Region"
}

variable "client_secret" {
  type        = string
  description = "Client Secret of the App Registration"

}
